# Te Emprestei

## É uma aplicação **Open-Source** escrita em java que vai te ajudar a gerenciar seus objetos emprestados.
### Nos dias atuais, é comum o empréstimo de objetos: livros, cd’s e entre outros. Também, é provável o esquecimento a quem foi emprestado o mesmo. Diante deste problema,o projeto de sistema Te Emprestei, ajuda com esta problemática de forma a melhorar a organização dos objetos pessoais. Este software tem como objetivo auxiliar na gerência e organização dos objetos emprestados, lembrando quando, o que, e a quem foi emprestado.

# Dependências

## Java 1.7 ou maior

# Baixe a última versão 
## [< Acesse aqui as Releases da aplicação >](https://gitlab.com/jsolemos/faculdade-emprestei/tags/1.0.2#release-102)

# Usar a aplicação

## Depois de feito o download do arquivo .jar, é só executar usando o Java.