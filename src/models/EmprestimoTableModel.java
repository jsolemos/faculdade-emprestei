/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import util.AppUtil;

/**
 *
 * @author jandersonlemos
 */
public class EmprestimoTableModel extends AbstractTableModel{
    private List<Serializable> dados;
    private String[] colunas = {"Objeto", "Portador", "Emprestado em", "Devolução Prevista para", "Devolução"};

    public EmprestimoTableModel() {
        this.dados = new ArrayList<>();
    }

    public List<Serializable> getDados() {
        return dados;
    }

    public void setDados(List<Serializable> dados) {
        this.dados = dados;
    }
    
    public void addRow(Emprestimo emprestimo){
        this.dados.add(emprestimo);
        this.fireTableDataChanged();
    }
    
    public String getColumnName(int num){
        return this.colunas[num];
    }

    @Override
    public int getRowCount() {
        return this.dados.size();
    }

    @Override
    public int getColumnCount() {
        return this.colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Emprestimo emprestimo = (Emprestimo) dados.get(rowIndex);
        switch(columnIndex){
            case 0: return emprestimo.getDescricaoObjeto();
            case 1: return emprestimo.getUsuario().getNome()+" - "+emprestimo.getUsuario().getTelefone();
            case 2: return AppUtil.dateToString(emprestimo.getDataEmprestimo());
            case 3: return AppUtil.dateToString(emprestimo.getDataPrevisaoDevolucao());
            case 4: return emprestimo.getDataDevolucao()!=null ? AppUtil.dateToString(emprestimo.getDataDevolucao()) : "Não devolvido";
            default: return null;
        }
    }
    
}
