/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import util.AppUtil;

/**
 *
 * @author jandersonlemos
 */
public class UsuarioComboBoxModel extends AbstractListModel<Usuario> implements ComboBoxModel<Usuario>{
    List<Serializable> lista;
    Usuario selected = null;
    
    public UsuarioComboBoxModel() {
        lista = new ArrayList<>();
        lista.add(new Usuario(null,"Selecione",""));
    }

    public List<Serializable> getLista() {
        return lista;
    }

    public void setLista(List<Serializable> lista) {
        this.lista = lista;
    }
    
    @Override
    public int getSize() {
        return this.lista.size();
    }

    @Override
    public Usuario getElementAt(int index) {
        return (Usuario)lista.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        this.selected = AppUtil.buscaUsuarioNaLista((Usuario)anItem, lista);
    }

    @Override
    public Object getSelectedItem() {
        return this.selected;
    }
    
}
