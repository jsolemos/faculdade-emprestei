/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import listeners.UiListener;
import dao.UsuarioDAO;
import exceptions.NegocioException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import listeners.AcoesListener;
import models.Emprestimo;
import models.Usuario;
import models.UsuarioComboBoxModel;
import rn.Aplicacao;
import util.AppUtil;

/**
 *
 * @author jandersonlemos
 */
public class EmprestarObjetoUI extends javax.swing.JPanel implements UiListener {
    private Emprestimo emprestimoEditar;
    private final String LABEL_CANCELAR = "Cancelar";

    public void setAcoesListener(AcoesListener acoesListener) {
        this.acoesListener = acoesListener;
    }
    private AcoesListener acoesListener;

    public Emprestimo getEmprestimoEditar() {
        return emprestimoEditar;
    }

    public void setEmprestimoEditar(Emprestimo emprestimoEditar) {
        this.emprestimoEditar = emprestimoEditar;
        this.btnLimpar.setText(LABEL_CANCELAR);
    }
    private String pessoas[];
    private JComboBox<Usuario> selectUsuario;

    /**
     * Creates new form EmprestarObjetoUI
     */
    public EmprestarObjetoUI() {
        initComponents();
        init();
    }

    private void init() {
        JButton btnAddUsuario = new JButton("Adicionar");
        btnAddUsuario.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                addUsuario();
            }
        });
        selectUsuario = new JComboBox<>();
        Dimension d = selectUsuario.getPreferredSize();
        selectUsuario.setPreferredSize(new Dimension(165, d.height));
        popularSelectPessoa();
        panelSelectPessoa.add(selectUsuario);
        panelSelectPessoa.add(btnAddUsuario);

        //Set Datas iniciais
        Date now = new Date();
        txtDataEmprestimo.setText(AppUtil.dateToString(now));
        txtDataPrevDevolucao.setText(AppUtil.dateToString(now));

    }
    

    private void preencheCamposEditar() {
        Usuario userSelect = AppUtil.buscaUsuarioNaLista(emprestimoEditar.getUsuario(), 
                                    ((UsuarioComboBoxModel)selectUsuario.getModel()).getLista());
        selectUsuario.setSelectedItem(userSelect);
        selectUsuario.validate();
        selectUsuario.repaint();
        txtDescricaoObj.setText(emprestimoEditar.getDescricaoObjeto());
        txtDataEmprestimo.setText(AppUtil.dateToString(emprestimoEditar.getDataEmprestimo()));
        txtDataPrevDevolucao.setText(AppUtil.dateToString(emprestimoEditar.getDataPrevisaoDevolucao()));
    }

    private void addUsuario() {
        UsuarioUI telaUsuario = new UsuarioUI();
        telaUsuario.setTitle("Emprestei");
        telaUsuario.setBackground(Color.WHITE);
        telaUsuario.setMaximumSize(new java.awt.Dimension(700, 400));
        telaUsuario.setMinimumSize(new java.awt.Dimension(700, 400));
        telaUsuario.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        telaUsuario.setVisible(true);
        telaUsuario.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                Logger.getLogger(EmprestarObjetoUI.class.getName()).log(Level.INFO, null, " windowClosed ");
                popularSelectPessoa();
                selectUsuario.repaint();
                super.windowClosed(e); //To change body of generated methods, choose Tools | Templates.
            }

        });
    }

    private void popularSelectPessoa() {
        try {
            UsuarioDAO dao = new UsuarioDAO();
            UsuarioComboBoxModel model = new UsuarioComboBoxModel();
            model.setLista(dao.listar());
            selectUsuario.setModel(model);
            selectUsuario.validate();
            selectUsuario.repaint();
        } catch (NegocioException ex) {
            exibeErroTela(ex);
        }
    }

    private void centralizarForm(ComponentEvent e) {
        Dimension sizes = e.getComponent().getBounds().getSize();
        int x = ((sizes.width / 2) - panelForm.getWidth() / 2);
        int xTituloTela = ((sizes.width / 2) - tituloTela.getWidth() / 2);

        panelForm.setLocation(x, panelForm.getLocation().y);
        tituloTela.setLocation(xTituloTela, tituloTela.getLocation().y);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tituloTela = new javax.swing.JLabel();
        panelForm = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        panelSelectPessoa = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescricaoObj = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        txtDataEmprestimo = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        txtDataPrevDevolucao = new javax.swing.JFormattedTextField();
        btnLimpar = new javax.swing.JButton();
        btnCadastrar = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(432, 578));
        setLayout(null);

        tituloTela.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        tituloTela.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tituloTela.setText("Emprestar Objetos");
        add(tituloTela);
        tituloTela.setBounds(360, 40, 270, 30);

        panelForm.setMaximumSize(null);
        panelForm.setLayout(new java.awt.GridLayout(6, 2, 10, 10));

        jLabel1.setText("Selecione a pessoa");
        panelForm.add(jLabel1);
        panelForm.add(panelSelectPessoa);

        jLabel2.setText("Descrição dos objetos:");
        panelForm.add(jLabel2);

        jScrollPane1.setPreferredSize(new java.awt.Dimension(222, 140));

        txtDescricaoObj.setColumns(20);
        txtDescricaoObj.setRows(6);
        jScrollPane1.setViewportView(txtDescricaoObj);

        jScrollPane1.setPreferredSize(new java.awt.Dimension(222, 300));

        panelForm.add(jScrollPane1);

        jLabel3.setText("Data do Emprestimo:");
        panelForm.add(jLabel3);

        txtDataEmprestimo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));
        panelForm.add(txtDataEmprestimo);

        jLabel4.setText("Data da previsão de devolução:");
        panelForm.add(jLabel4);

        txtDataPrevDevolucao.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));
        panelForm.add(txtDataPrevDevolucao);

        btnLimpar.setText("Limpar");
        btnLimpar.setActionCommand("cadastrarObjeto");
        btnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparActionPerformed(evt);
            }
        });
        panelForm.add(btnLimpar);

        btnCadastrar.setText("Cadastrar ");
        btnCadastrar.setActionCommand("cadastrarObjeto");
        btnCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarActionPerformed(evt);
            }
        });
        panelForm.add(btnCadastrar);

        add(panelForm);
        panelForm.setBounds(190, 120, 590, 390);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarActionPerformed
        try {
            Emprestimo emprestimo = null;
           
            if(emprestimoEditar != null){
                emprestimo = emprestimoEditar;
            }else{
                emprestimo = new Emprestimo();
            }
            
            emprestimo.setDescricaoObjeto(txtDescricaoObj.getText());

            if (!"".equals(txtDataEmprestimo.getText())) {
                emprestimo.setDataEmprestimo(AppUtil.stringToDate(txtDataEmprestimo.getText()));
            }

            if (!"".equals(txtDataPrevDevolucao.getText())) {
                emprestimo.setDataPrevisaoDevolucao(AppUtil.stringToDate(txtDataPrevDevolucao.getText()));
            }

            emprestimo.setUsuario((Usuario) selectUsuario.getModel().getSelectedItem());
            
            Aplicacao app = Aplicacao.getInstance();

            if (app.emprestarObjeto(emprestimo)) {
                if(acoesListener!=null){
                    acoesListener.aoAtualizar();
                    acoesListener = null;
                    emprestimoEditar = null;
                }
                JOptionPane.showMessageDialog(null, "Emprestimo Salvo com sucesso.");
                limparForm();
                this.setVisible(false);
            }

        } catch (NegocioException ex) {
            exibeErroTela(ex);
        }
    }//GEN-LAST:event_btnCadastrarActionPerformed

    private void exibeErroTela(NegocioException ex) throws HeadlessException {
        Logger.getLogger(EmprestarObjetoUI.class.getName()).log(Level.SEVERE, null, ex);
        JOptionPane.showMessageDialog(null, ex.getMessage());
    }

    private void btnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparActionPerformed
        limparForm();
    }//GEN-LAST:event_btnLimparActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCadastrar;
    private javax.swing.JButton btnLimpar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelForm;
    private javax.swing.JPanel panelSelectPessoa;
    private javax.swing.JLabel tituloTela;
    private javax.swing.JFormattedTextField txtDataEmprestimo;
    private javax.swing.JFormattedTextField txtDataPrevDevolucao;
    private javax.swing.JTextArea txtDescricaoObj;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onResize(ComponentEvent e) {
        centralizarForm(e);
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag); //To change body of generated methods, choose Tools | Templates.
        if(aFlag){
            if(this.emprestimoEditar != null){
                btnCadastrar.setText("Editar");
                preencheCamposEditar();
            }else{
                btnCadastrar.setText("Cadastrar");
            }
        }
    }
    
    public void limparForm() {
        this.txtDescricaoObj.setText("");
        if(selectUsuario != null && selectUsuario.getModel() != null){
            popularSelectPessoa();
            selectUsuario.validate();
            selectUsuario.repaint();
        }
        if(this.btnLimpar.getText().equals(LABEL_CANCELAR)){
            PrincipalUI pui = PrincipalUI.instance;            
            JPanel panel = (JPanel) pui.panels.get("Emprestimos");
            pui.alternarTela(panel);
            
            if(panel instanceof EmprestarObjetoUI){
                ((EmprestarObjetoUI) panel).limparForm();
            }
            pui.pack();
            this.btnLimpar.setText("Limpar");
        }
    }
}
