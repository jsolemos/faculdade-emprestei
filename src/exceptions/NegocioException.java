/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author jandersonlemos
 */
public class NegocioException extends Exception{
    public static enum TIPO {
       NEGOCIO, SQL, OUTROS;
    } 
    
    private TIPO tipo;
    
    public NegocioException(String message, TIPO tipo, Throwable cause) {
        super(message, cause);
        this.tipo = tipo;
    }
    
    public NegocioException(String message, TIPO tipo) {
        super(message);
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo.name();
    }

    public void setTipo(TIPO tipo) {
        this.tipo = tipo;
    }
    
}
