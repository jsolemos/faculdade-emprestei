/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import exceptions.NegocioException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Usuario;

/**
 *
 * @author jandersonlemos
 */
public class AppUtil {
    
    public static Date stringToDateDB(String strDate) throws NegocioException{
        if(strDate == null) return null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        try {
            return sdf.parse(strDate);
        } catch (ParseException ex) {
            Date d = new Date();
            d.setTime(Long.parseLong(strDate));
            return d;
        }
    }
    
    public static Date stringToDate(String strDate) throws NegocioException{
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        try {
            return sdf.parse(strDate);
        } catch (ParseException ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);            
            throw new NegocioException("Formato da Data inválida. Utilize: dd/mm/yyyy", NegocioException.TIPO.NEGOCIO);
        }
    }
    
    public static String dateToString(Date date){
        if(date == null) return "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }
    
    public static String dateToStringDB(Date date){
        if(date == null) return "NULL";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static Usuario buscaUsuarioNaLista(Usuario usuario, List<Serializable> lista) {
        if(usuario == null) return null;
        Usuario retorno = null;
        for(Serializable sU : lista){
            Usuario u = (Usuario)sU;
            if(usuario.equals(u)){
                retorno = u;
                break;
            }
        }
        return retorno;
    }
    
}
