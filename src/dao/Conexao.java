/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jandersonlemos
 */
public class Conexao {
    
    private final String SERVER_NAME = "localhost";
    private final String DB = "emprestei";
    private final String USER = "root";
    private final String PASS = "root";
    private Connection connection;
    
    public Connection getConnection() throws SQLException {
        if(connection!=null && !connection.isClosed()) return null;
        
        try {
            

            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:db."+DB);
            
//            Class.forName("com.mysql.jdbc.Driver");
//            String url = "jdbc:mysql://" + SERVER_NAME + "/" + DB;
//            connection = DriverManager.getConnection(url, USER, PASS);
            
            Logger.getLogger(Conexao.class.getName()).log(Level.INFO, "Iniciando Conex\u00e3o com {0} pelo usuario {1}", new Object[]{SERVER_NAME, USER});
            initDB();
            return connection;

        } catch (ClassNotFoundException ex) { 
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, "O driver expecificado nao foi encontrado."+ex.getMessage(), ex);
            return null;
        }
    }

    private void initDB() {
        try {
            Statement stmt = connection.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS \"usuario\"(" +
                        "  \"id_usuario\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                        "  \"nome_usuario\" VARCHAR(30) NOT NULL," +
                        "  \"telefone_usuario\" VARCHAR(30) NOT NULL" +
                        ");" +
                        "CREATE TABLE IF NOT EXISTS \"emprestimo\"(" +
                        "  \"id_emprestimo\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                        "  \"id_usuario\" INTEGER NOT NULL," +
                        "  \"descricao_objeto_emprestimo\" VARCHAR(50) DEFAULT NULL," +
                        "  \"data_emprestimo\" DATE NOT NULL," +
                        "  \"data_previsao_devolucao_emprestimo\" DATE NOT NULL," +
                        "  \"data_devolucao_emprestimo\" DATE DEFAUaLT NULL," +
                        "  CONSTRAINT \"emprestimo_usuario_FK\"" +
                        "    FOREIGN KEY(\"id_usuario\")" +
                        "    REFERENCES \"usuario\"(\"id_usuario\")" +
                        "    ON DELETE CASCADE" +
                        "    ON UPDATE CASCADE" +
                        ");" +
                        "CREATE INDEX IF NOT EXISTS \"emprestimo.id_usuario\" ON \"emprestimo\" (\"id_usuario\");";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    

}
