/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import exceptions.NegocioException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jandersonlemos
 */
public abstract class DAO{
    protected Connection con;
    public DAO() throws NegocioException {
        try {
            this.con = new Conexao().getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            throw new NegocioException("Problemas ao Iniciar conexao com o banco. "+ex.getMessage(), NegocioException.TIPO.SQL, ex.getCause());
        }
    }
    
    public abstract boolean inserir(Serializable obj) throws NegocioException;;
    public abstract boolean editar(Serializable obj) throws NegocioException;
    public abstract List<Serializable> listar() throws NegocioException;
    public abstract boolean excluir(Serializable obj) throws NegocioException;
}
