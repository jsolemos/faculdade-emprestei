/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import exceptions.NegocioException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Usuario;

/**
 *
 * @author jandersonlemos
 */
public class UsuarioDAO extends DAO{
    

    public UsuarioDAO() throws NegocioException {
        super();
    }

    @Override
    public boolean inserir(Serializable obj) throws NegocioException {
        Usuario usuario = (Usuario) obj;
        String sqlInsetUsuario = "INSERT INTO `usuario` (" +
                                "`id_usuario` ," +
                                "`nome_usuario` ," +
                                "`telefone_usuario`" +
                                ")" +
                                "VALUES (" +
                                "NULL ,  ?,  ?" +
                                ");";
        
        try {
            PreparedStatement stmt = con.prepareStatement(sqlInsetUsuario);
            stmt.setString(1, usuario.getNome());
            stmt.setString(2, usuario.getTelefone());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            throw new NegocioException("Ocorreu um Problema ao Salvar o Emprestimo.", NegocioException.TIPO.SQL, ex.getCause());
        }
    }

    @Override
    public boolean editar(Serializable obj) throws NegocioException {
        Usuario usuario = (Usuario) obj;
        String sql = "UPDATE `usuario` "+
                " SET `nome_usuario`= ? ," +
                " `telefone_usuario` = ? WHERE id_usuario = ?";
        
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setLong(1, usuario.getId());
            stmt.setString(2, usuario.getNome());
            stmt.setString(3, usuario.getTelefone());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            throw new NegocioException("Ocorreu um Problema ao Salvar o Emprestimo.", NegocioException.TIPO.SQL, ex.getCause());
        }
    }

    @Override
    public List<Serializable> listar() throws NegocioException {
        List<Serializable> lista = new ArrayList<>();
        try {
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM usuario");
            ResultSet res = stmt.executeQuery();
            while(res.next()){
                Usuario u = new Usuario();
                u.setId(res.getLong("id_usuario"));
                u.setNome(res.getString("nome_usuario"));
                u.setTelefone(res.getString("telefone_usuario"));
                lista.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lista;
    }

    @Override
    public boolean excluir(Serializable obj) throws NegocioException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    
}
