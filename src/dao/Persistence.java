/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import exceptions.NegocioException;
import java.io.Serializable;
import java.util.Date;
import models.Usuario;

/**
 *
 * @author jandersonlemos
 */
public class Persistence implements IPersistence{

    @Override
    public boolean emprestar(Serializable obj) throws NegocioException {
        DAO dao = new EmprestimoDAO();
        dao.inserir(obj);
        return true;
    }

    @Override
    public boolean atualizarEmprestimo(Serializable obj) throws NegocioException {
        EmprestimoDAO dao = new EmprestimoDAO();
        return dao.editar(obj);
    }

    @Override
    public boolean excluirEmprestimo(Serializable obj) throws NegocioException {
        EmprestimoDAO dao = new EmprestimoDAO();
        return dao.excluir(obj);
    }

    
    @Override
    public boolean salvarUsuario(Serializable usuario) throws NegocioException {
        UsuarioDAO dao = new UsuarioDAO();
        if(((Usuario) usuario).getId() == null)
            return dao.inserir(usuario);
        else
            return dao.editar(usuario);
    }
    
}
