/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import exceptions.NegocioException;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author jandersonlemos
 */
public interface IPersistence {
    public boolean emprestar(Serializable obj) throws NegocioException;
    public boolean atualizarEmprestimo(Serializable obj) throws NegocioException;
    public boolean excluirEmprestimo(Serializable obj) throws NegocioException;
    public boolean salvarUsuario(Serializable obj) throws NegocioException;
}
