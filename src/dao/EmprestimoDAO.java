/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import exceptions.NegocioException;
import java.io.Serializable;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Emprestimo;
import models.Usuario;
import util.AppUtil;

/**
 *
 * @author jandersonlemos
 */
public class EmprestimoDAO extends DAO {

    public EmprestimoDAO() throws NegocioException {
        super();
    }

    private Timestamp getDateTime(Date d) {
        return new Timestamp(d.getTime());
    }

    @Override
    public boolean inserir(Serializable obj) throws NegocioException {
        Emprestimo emprestimo = (Emprestimo) obj;
        String sqlInsetObjeto = "INSERT INTO  emprestimo ("
                + "`id_emprestimo` ,"
                + "`id_usuario` ,"
                + "`descricao_objeto_emprestimo` ,"
                + "`data_emprestimo` ,"
                + "`data_previsao_devolucao_emprestimo` ,"
                + "`data_devolucao_emprestimo`"
                + ") "
                + "VALUES ( "
                + "NULL,  ?,  ?,  ?,  ?, NULL"
                + ");";
        try {
            PreparedStatement stmt = con.prepareStatement(sqlInsetObjeto);
            stmt.setLong(1, emprestimo.getUsuario().getId());
            stmt.setString(2, emprestimo.getDescricaoObjeto());
            stmt.setDate(3, Date.valueOf(AppUtil.dateToStringDB(emprestimo.getDataEmprestimo())));
            stmt.setDate(4, Date.valueOf(AppUtil.dateToStringDB(emprestimo.getDataPrevisaoDevolucao())));
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(EmprestimoDAO.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            throw new NegocioException("Ocorreu um Problema ao Salvar o Emprestimo.", NegocioException.TIPO.SQL, ex.getCause());
        }
    }

    @Override
    public boolean editar(Serializable obj) throws NegocioException {
        Emprestimo emprestimo = (Emprestimo) obj;
        String sql = "UPDATE emprestimo "
                + " SET id_usuario=?,"
                + " descricao_objeto_emprestimo=?,"
                + " data_emprestimo=?,"
                + " data_previsao_devolucao_emprestimo=?, "
                + " data_devolucao_emprestimo=? "
                + " WHERE id_emprestimo=?;";
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setLong(1, emprestimo.getUsuario().getId());
            stmt.setString(2, emprestimo.getDescricaoObjeto());
            stmt.setDate(3, Date.valueOf(AppUtil.dateToStringDB(emprestimo.getDataEmprestimo())));
            stmt.setDate(4, Date.valueOf(AppUtil.dateToStringDB(emprestimo.getDataPrevisaoDevolucao())));
            stmt.setDate(5, emprestimo.getDataDevolucao() == null ? null : Date.valueOf(AppUtil.dateToStringDB(emprestimo.getDataDevolucao())));
            stmt.setLong(6, emprestimo.getId());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(EmprestimoDAO.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            throw new NegocioException("Ocorreu um Problema ao Editar o Emprestimo.", NegocioException.TIPO.SQL, ex.getCause());
        }
    }

    @Override
    public List<Serializable> listar() throws NegocioException {
        List<Serializable> lista = new ArrayList<>();
        try {
            PreparedStatement stmt = con.prepareStatement("SELECT e.id_emprestimo, e.id_usuario, e.descricao_objeto_emprestimo, e.data_emprestimo,e.data_devolucao_emprestimo, e.data_previsao_devolucao_emprestimo,u.nome_usuario,u.telefone_usuario\n"
                    + "FROM emprestimo e inner join usuario u on e.id_usuario = u.id_usuario order by e.data_emprestimo");
            ResultSet res = stmt.executeQuery();
            while (res.next()) {
                Emprestimo emprestimo;
                emprestimo = new Emprestimo(
                        res.getLong("id_emprestimo"),
                        res.getString("descricao_objeto_emprestimo"),
                        AppUtil.stringToDateDB(res.getString("data_emprestimo")),
                        AppUtil.stringToDateDB(res.getString("data_previsao_devolucao_emprestimo")),
                        AppUtil.stringToDateDB(res.getString("data_devolucao_emprestimo"))
                );
                Usuario usuario = new Usuario(res.getLong("id_usuario"), res.getString("nome_usuario"), res.getString("telefone_usuario"));
                emprestimo.setUsuario(usuario);

                lista.add(emprestimo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmprestimoDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw new NegocioException("Ocorreu ao consultar emprestimos.", NegocioException.TIPO.SQL, ex.getCause());
        }
        return lista;
    }

    @Override
    public boolean excluir(Serializable obj) throws NegocioException {
        Emprestimo emprestimo = (Emprestimo) obj;
        String sql = "DELETE FROM emprestimo WHERE id_emprestimo=?";
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setLong(1, emprestimo.getId());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(EmprestimoDAO.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            throw new NegocioException("Ocorreu um Problema ao Editar o Emprestimo.", NegocioException.TIPO.SQL, ex.getCause());
        }
    }

}
