/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rn;

import dao.Conexao;
import dao.Persistence;
import exceptions.NegocioException;
import models.Emprestimo;
import models.Usuario;

/**
 *
 * @author jandersonlemos
 */
public final class Aplicacao implements INegocio{
    
    private final Conexao con;
    private static Aplicacao instance; 
    
    public static Aplicacao getInstance(){
        if(instance == null){
            instance = new Aplicacao();
        }
        return instance;
    }
    
    private Aplicacao() {
        this.con = new Conexao();
    }
    
    private boolean isNull(String str) {
        return "".equals(str);
    }
    
    private boolean telefoneEhValido(String str){
        return !isNull(str) && str.length() == 10;
    }
    

    @Override
    public boolean emprestarObjeto(Emprestimo emprestimo) throws NegocioException{
        if(isNull(emprestimo.getDescricaoObjeto())){
            throw new NegocioException("Preencha a descricao do Objeto", NegocioException.TIPO.NEGOCIO);
        } else if (emprestimo.getUsuario() == null && emprestimo.getUsuario().getId() == null){
            throw new NegocioException("Preencha corretamente os dados do usuário", NegocioException.TIPO.NEGOCIO);
        }
        
        Persistence persistence = new Persistence();
        if(emprestimo.getId()== null)
            return persistence.emprestar(emprestimo);
        else 
            return persistence.atualizarEmprestimo(emprestimo);
    }

    @Override
    public boolean cadastrarUsuario(Usuario usuario) throws NegocioException {
        if(isNull(usuario.getNome()) || isNull(usuario.getTelefone())){
            throw new NegocioException("Preencha os dados corretamente.", NegocioException.TIPO.NEGOCIO);
        }
        Persistence persistence = new Persistence();
        return persistence.salvarUsuario(usuario);
    }

    @Override
    public boolean receberObjeto(Emprestimo emprestimo) throws NegocioException {
        if(emprestimo.getDataDevolucao() == null){
            throw new NegocioException("Informe a data de devolução", NegocioException.TIPO.NEGOCIO);
        }
        Persistence persistence = new Persistence();
        return persistence.atualizarEmprestimo(emprestimo);
    }

    @Override
    public boolean excluirEmprestimo(Emprestimo objeto) throws NegocioException {
        Persistence persistence = new Persistence();
        return persistence.excluirEmprestimo(objeto);
    }

    @Override
    public boolean editarEmprestimo(Emprestimo emprestimo) throws NegocioException {
        if(isNull(emprestimo.getDescricaoObjeto())){
            throw new NegocioException("Preencha a descricao do Objeto", NegocioException.TIPO.NEGOCIO);
        } else if (emprestimo.getUsuario() == null && emprestimo.getUsuario().getId() == null){
            throw new NegocioException("Preencha corretamente os dados do usuário", NegocioException.TIPO.NEGOCIO);
        }
        Persistence persistence = new Persistence();
        return persistence.atualizarEmprestimo(emprestimo);
    }
    
}
