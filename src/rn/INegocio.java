/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rn;

import exceptions.NegocioException;
import models.Emprestimo;
import models.Usuario;

/**
 *
 * @author jandersonlemos
 */
public interface INegocio {
    public boolean emprestarObjeto(Emprestimo objeto) throws NegocioException;
    public boolean receberObjeto(Emprestimo objeto) throws NegocioException;
    public boolean excluirEmprestimo(Emprestimo objeto) throws NegocioException;
    public boolean editarEmprestimo(Emprestimo objeto) throws NegocioException;
    public boolean cadastrarUsuario(Usuario usuario) throws NegocioException;
    
}
