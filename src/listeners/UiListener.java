/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listeners;

import java.awt.event.ComponentEvent;
import javax.swing.JFrame;

/**
 *
 * @author jandersonlemos
 */
public interface UiListener {
    public void onResize(ComponentEvent e);
}
